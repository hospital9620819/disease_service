package service

import (
	"context"
	pb "hospital/disease_service/genproto/illines_service"
	l "hospital/disease_service/pkg/logger"
	grpcClient "hospital/disease_service/service/grpc_client"
	"hospital/disease_service/storage"
	"log"

	"github.com/jmoiron/sqlx"
)

// Disease Service ...
type IllinesService struct {
	storage storage.IStorage
	logger  l.Logger
	Client  grpcClient.Clients
}

func NewIllinesService(db *sqlx.DB, log l.Logger, client grpcClient.Clients) *IllinesService {
	return &IllinesService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		Client:  client,
	}
}

// Medications ...
func (i *IllinesService) CreateMedication(ctx context.Context, req *pb.MedicationsReq) (*pb.MedicationsReq, error) {
	illinesName := &pb.IllinesName{
		IllinesName: req.IllinessName,
	}

	resIllinesId, err := i.storage.Illiness().GetIllinesIdByNameForMedication(illinesName)
	if err != nil {
		log.Println("error get illines id : ", err)
	}

	req.IllinessId = resIllinesId.IllinesId
	res, err := i.storage.Illiness().CreateMedication(req)
	if err != nil {
		log.Println("failed to creating medication : ", err)
	}

	return res, nil
}

func (i *IllinesService) GetMedicationById(ctx context.Context, req *pb.IllinesName) (*pb.IllinesId, error) {
	res, err := i.storage.Illiness().GetMedicationById(req)
	if err != nil {
		log.Println("failed to get medication by id : ", err)
	}

	return res, nil
}

func (i *IllinesService) GetMedicationByIllinesName(ctx context.Context, req *pb.IllinesName) (*pb.MedicationsReq, error) {
	res, err := i.storage.Illiness().GetMedicationByIllinesName(req)
	if err != nil {
		log.Println("failed to get medication by name : ", err)
	}
	return res, nil
}

func (i *IllinesService) GetMedicationInfoList(ctx context.Context, req *pb.MedicationList) (*pb.MedicationsList, error) {
	res, err := i.storage.Illiness().GetMedicationInfoList(req)
	if err != nil {
		log.Println("failed to get medications info list : ", err)
	}
	return res, nil
}

func (i *IllinesService) UpdateMetication(ctx context.Context, req *pb.UpdateMed) (*pb.MedicationsReq, error) {
	res, err := i.storage.Illiness().UpdateMetication(req)
	if err != nil {
		log.Println("failed to update medication : ", err)
	}

	return res, nil
}

func (i *IllinesService) DeleteMetication(ctx context.Context, req *pb.MedicationsName) (*pb.MedicationsReq, error) {
	res, err := i.storage.Illiness().DeleteMetication(req)

	if err != nil {
		log.Println("failed to delete medication : ", err)
	}

	return res, nil
}

// Diseases ...
func (i *IllinesService) CreateIlliness(ctx context.Context, req *pb.Illiness) (*pb.IllinessRes, error) {
	res, err := i.storage.Illiness().CreateIlliness(req)
	if err != nil {
		log.Println("error to create illiness info : ", err)
	}

	return res, nil
}

func (i *IllinesService) GetIllinesInfoByName(ctx context.Context, req *pb.IllinesName) (*pb.Illiness, error) {
	res, err := i.storage.Illiness().GetIllinesInfoByName(req)

	if err != nil {
		log.Println("failed to get illiness : ", err)
	}

	return res, nil
}

func (i *IllinesService) GetIllinesInfoList(ctx context.Context, req *pb.IllinessList) (*pb.IllinessesList, error) {
	res, err := i.storage.Illiness().GetIllinesInfoList(req)

	if err != nil {
		log.Panicln("failed fo get all illiness info : ", err)
	}

	return res, nil
}

func (i *IllinesService) UpdateIllines(ctx context.Context, req *pb.UpdateIllRes) (*pb.Illiness, error) {
	res, err := i.storage.Illiness().UpdateIllines(req)

	if err != nil {
		log.Println("failed to update illines : ", err)
	}

	return res, nil
}

func (i *IllinesService) DeleteIlliness(ctx context.Context, req *pb.IllinesName) (*pb.Illiness, error) {
	res, err := i.storage.Illiness().DeleteIlliness(req)

	if err != nil {
		log.Println("failed to delete illiness : ",err)
	}

	return res, nil
}
