package grpcclient

import (
	"fmt"
	"hospital/disease_service/config"
	p "hospital/disease_service/genproto/patient"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)


type Clients interface {
	Patient() p.PatientServiceClient
}

type ServiceManager struct {
	Config config.Config
	patientService p.PatientServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	connPatient, err := grpc.Dial(
		fmt.Sprintf("%s:%s",cfg.PatientServiceHost, cfg.PatientServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	
	if err != nil {
		return nil, fmt.Errorf("patient service dial host : %s port : %s ", cfg.PatientServiceHost, cfg.PatientServicePort)
	}

	return &ServiceManager{
		Config: cfg,
		patientService: p.NewPatientServiceClient(connPatient),
	}, nil
}

func (s *ServiceManager) Patient() p.PatientServiceClient {
	return s.patientService
}
