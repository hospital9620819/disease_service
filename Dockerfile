FROM golang:1.17-alpine
RUN mkdir disease_service
COPY . /disease_service
WORKDIR /disease_service
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 6000