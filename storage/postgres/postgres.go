package postgres

import (
	"github.com/jmoiron/sqlx"
)

type IllinessRepo struct {
	db *sqlx.DB
}

func NewIllinessRepo(db *sqlx.DB) *IllinessRepo {
	return &IllinessRepo{
		db: db,
	}
}