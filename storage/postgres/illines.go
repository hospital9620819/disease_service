package postgres

import (
	"fmt"
	pb "hospital/disease_service/genproto/illines_service"
	"log"
	"time"
)

// service  (*emptypb.Empty, error)
// 	"google.golang.org/protobuf/types/known/emptypb"

// Medication ...
func (i *IllinessRepo) CreateMedication(medication *pb.MedicationsReq) (*pb.MedicationsReq, error) {
	var res pb.MedicationsReq
	err := i.db.DB.QueryRow(`
		INSERT INTO medications(
			medicine_name, dossage, acceptance, bad_effects, ok_effects, illiness_name, illiness_id)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
		RETURNING 
		    medication_id ,medicine_name, dossage, acceptance, bad_effects,
			ok_effects, illiness_name, illiness_id`,
		medication.MedicineName, medication.Dossage, medication.Acceptance,
		medication.BadEffects, medication.OkEffects, medication.IllinessName, medication.IllinessId).
		Scan(
			&res.MedicationId, &res.MedicineName, &res.Dossage, &res.Acceptance,
			&res.BadEffects, &res.OkEffects, &res.IllinessName, &res.IllinessId)

	if err != nil {
		log.Println("failed to create medication: ", err)
	}

	return &res, nil
}

func (i *IllinessRepo) GetMedicationById(ilName *pb.IllinesName) (*pb.IllinesId, error) {
	var res pb.IllinesId
	err := i.db.DB.QueryRow(`
	    SELECT medication_id
		FROM 
			medications
		WHERE 
			illiness_name = $1`, ilName.IllinesName).Scan(&res.IllinesId)
	if err != nil {
		log.Println("failed to get medication : ", err)
	}

	return &res, nil
}

func (i *IllinessRepo) GetMedicationByIllinesName(ilName *pb.IllinesName) (*pb.MedicationsReq, error) {
	var res pb.MedicationsReq
	err := i.db.DB.QueryRow(`
	    SELECT 
			medicine_name, dossage, acceptance, bad_effects, ok_effects, illiness_name
		FROM 
			medications
		WHERE 
			illiness_name = $1`, ilName.IllinesName).Scan(
		&res.MedicineName, &res.Dossage, &res.Acceptance, &res.BadEffects,
		&res.OkEffects, &res.IllinessId)
	if err != nil {
		log.Println("failed to get medication : ", err)
	}

	return &res, nil
}

func (i *IllinessRepo) GetMedicationInfoList(list *pb.MedicationList) (*pb.MedicationsList, error) {
	var res []*pb.MedicationsReq
	offset := (list.Page - 1) * list.Limit

	rows, err := i.db.DB.Query(`
		SELECT medicine_name, dossage, acceptance, bad_effects, ok_effects, illiness_name
		FROM medications
		LIMIT $1 OFFSET $2`, list.Limit, offset)

	if err != nil {
		log.Println("failed to get medications inso : ", err)
	}

	defer rows.Close()

	for rows.Next() {
		temp := pb.MedicationsReq{}
		err := rows.Scan(
			&temp.MedicineName,
			&temp.Dossage,
			&temp.Acceptance,
			&temp.BadEffects,
			&temp.OkEffects,
			&temp.IllinessId,
		)

		if err != nil {
			log.Println("failed to scan medication info : ", err)
		}

		res = append(res, &temp)
	}

	if err := rows.Err(); err != nil {
		log.Println("error occurred while iterating rows:", err)
		return nil, err
	}

	return nil, nil
}

func (i *IllinessRepo) UpdateMetication(req *pb.UpdateMed) (*pb.MedicationsReq, error) {
	var res pb.MedicationsReq
	err := i.db.DB.QueryRow(`
		UPDATE medications
		SET dossage = $1, acceptance = $2, bad_effects = $3
		WHERE medicine_name = $4
		RETURNING medicine_name, dossage, acceptance, bad_effects, ok_effects, illiness_name`,
		req.Dosage, req.Acceptance, req.BadEffects, req.MedicineName).
		Scan(&res.MedicineName, &res.Dossage, &res.Acceptance, &res.BadEffects,
			&res.OkEffects, &res.IllinessId)

	if err != nil {
		log.Println("fail to update medication : ", err)
	}

	return &res, nil
}

func (i *IllinessRepo) DeleteMetication(medName *pb.MedicationsName) (*pb.MedicationsReq, error) {
	var res pb.MedicationsReq
	err := i.db.DB.QueryRow(`
		DELETE FROM medications
		WHERE medicine_name = $1
		RETURNING medication_id ,medicine_name, dossage, acceptance, bad_effects,ok_effects, illiness_name
		`, medName.MedicineName).Scan(&res.MedicationId, &res.MedicineName, &res.Dossage, &res.Acceptance,
		&res.BadEffects, &res.OkEffects, &res.IllinessId)

	if err != nil {
		log.Println("failed to delete medication : ", err)
	}

	return &res, nil
}



// Illiness ...
func (i *IllinessRepo) CreateIlliness(il *pb.Illiness) (*pb.IllinessRes, error) {
	var res pb.IllinessRes

	err := i.db.DB.QueryRow(`
		INSERT INTO illiness
			(illiness_name, descriptions)
		VALUES ($1, $2)
		RETURNING 
			illiness_id, illiness_name, descriptions`, 
			il.IllinesName, il.Description).
		Scan(
			&res.IllinesId,
			&res.IllinesName,
			&res.Description,
		)

	if err != nil {
		log.Println("failed to create illiness info:", err)
	}

	return &res, err
}

func (i *IllinessRepo) GetIllinesIdByNameForMedication(ilName *pb.IllinesName) (*pb.IllinesId, error) {
	var res pb.IllinesId
	err := i.db.DB.QueryRow(`
		SELECT 
			illiness_id
		FROM
			illiness
		WHERE 
		illiness_name = $1`, ilName.IllinesName).Scan(&res.IllinesId)
	
	if err != nil {
		log.Println("error to get illines id:", err)
	}

	return &res, nil
}

func (i *IllinessRepo) GetIllinesInfoByName(ilName *pb.IllinesName) (*pb.Illiness, error) {
	var res pb.Illiness
	err := i.db.DB.QueryRow(`
	    SELECT illiness_name, descriptions, medications_info
		FROM illiness
		WHERE illiness_name = $1`, ilName.IllinesName).Scan(
		&res.IllinesName, &res.Description,
	)
	fmt.Println(">>>>>>>>> ", res)
	if err != nil {
		log.Println("failed to get illines by name : ", err)
	}

	return &res, err
}

func (i *IllinessRepo) GetIllinesInfoList(req *pb.IllinessList) (*pb.IllinessesList, error) {
	var response *pb.IllinessesList
	offset := (req.Page - 1) * req.Limit

	rows, err := i.db.DB.Query(`
		SELECT illiness_name, descriptions, medications_info
		FROM illiness
		LIMIT $1 OFFSET $2`, req.Limit, offset)

	if err != nil {
		log.Fatalln("fail to get all illiness info : ", err)
	}

	defer rows.Close()

	for rows.Next() {
		temp := pb.Illiness{}
		medicatInfo := pb.MedicationInfoForIlliness{}

		err := rows.Scan(
			&temp.IllinesName,
			&temp.Description,
			&medicatInfo.MedicateName,
			&medicatInfo.Dossage,
			&medicatInfo.Acceptance,
			&medicatInfo.BadEffects,
			&medicatInfo.OkEffects,
		)

		if err != nil {
			log.Printf("failed to scan illines info : ", err)
		}

		//response = append(response, &temp)
	}

	if err := rows.Err(); err != nil {
		log.Println("error occurred while iterating rows:", err)
		return nil, err
	}

	return response, nil
}

func (i *IllinessRepo) UpdateIllines(req *pb.UpdateIllRes) (*pb.Illiness, error) {
	var res pb.Illiness

	err := i.db.DB.QueryRow(`
		UPDATE illiness
		SET descriptions = $1
		WHERE illiness_name = $2
		RETURNING illiness_name, descriptions, medications_info`, req.Descriptions, req.IllinesName).
		Scan(&res.IllinesName, &res.Description)

	if err != nil {
		log.Println("failed to update illiness info : ", err)
	}

	return &res, nil
}

func (i *IllinessRepo) DeleteIlliness(req *pb.IllinesName) (*pb.Illiness, error) {
	var res pb.Illiness
	err := i.db.DB.QueryRow(`
		UPDATE illiness
		SET deleted_at = $1
		WHERE illiness_name = $2
		RETURNING illiness_name, descriptions, medications_info`, time.Now(), req.IllinesName).
		Scan(&res.IllinesName, &res.Description)

	if err != nil {
		log.Println("failed to delete illiness info : ", err)
	}

	return &res, nil
}
