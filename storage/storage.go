package storage

import (
	"hospital/disease_service/storage/postgres"
	"hospital/disease_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Illiness() repo.IllinessStorage
}

type storagePg struct {
	db          *sqlx.DB
	illinessRepo repo.IllinessStorage
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:          db,
		illinessRepo: postgres.NewIllinessRepo(db),
	}
}

func (s *storagePg) Illiness() repo.IllinessStorage {
	return s.illinessRepo
}
