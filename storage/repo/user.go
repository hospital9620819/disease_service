package repo

import (
	pb "hospital/disease_service/genproto/illines_service"
)

type IllinessStorage interface {
	// Medication ...
	CreateMedication(*pb.MedicationsReq) (*pb.MedicationsReq, error)
	GetMedicationById(*pb.IllinesName) (*pb.IllinesId, error)
	GetMedicationByIllinesName(*pb.IllinesName) (*pb.MedicationsReq, error)
	GetMedicationInfoList(*pb.MedicationList) (*pb.MedicationsList, error)
	UpdateMetication(*pb.UpdateMed) (*pb.MedicationsReq, error)
	DeleteMetication(*pb.MedicationsName) (*pb.MedicationsReq, error)

	// Disease ...
	CreateIlliness(*pb.Illiness) (*pb.IllinessRes, error)
	GetIllinesInfoByName(*pb.IllinesName) (*pb.Illiness, error)
	GetIllinesIdByNameForMedication(*pb.IllinesName) (*pb.IllinesId, error)
	GetIllinesInfoList(*pb.IllinessList) (*pb.IllinessesList, error)
	UpdateIllines(*pb.UpdateIllRes) (*pb.Illiness, error)
	DeleteIlliness(*pb.IllinesName) (*pb.Illiness, error)

}
