CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS medications(
    medication_id uuid DEFAULT uuid_generate_v4 (),
    medicine_name TEXT NOT NULL, 
    dossage TEXT NOT NULL,
    acceptance TEXT NOT NULL,
    bad_effects TEXT NOT NULL,
    ok_effects TEXT NOT NULL,
    illiness_name TEXT NOT NULL,
    illiness_id uuid DEFAULT uuid_generate_v4 ()
);


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS illiness(
    illiness_id uuid DEFAULT uuid_generate_v4 (),
    illiness_name TEXT NOT NULL,
    descriptions TEXT NOT NULL
);