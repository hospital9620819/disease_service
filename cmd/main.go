package main

// ........
import (
	"net"

	"hospital/disease_service/config"
	p "hospital/disease_service/genproto/illines_service"
	"hospital/disease_service/pkg/db"
	"hospital/disease_service/pkg/logger"
	"hospital/disease_service/service"
	grpcclient "hospital/disease_service/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "disease_service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", int(cfg.PostgresPort)),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error: %v", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Failed while grpc client:  %v", logger.Error(err))
	}

	illinesService := service.NewIllinesService(connDB, log, grpcClient)

	lis, err := net.Listen("tcp", cfg.IllinesServicePort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	p.RegisterDiseaseServiceServer(s, illinesService)

	log.Info("main: server running", logger.String("port", cfg.IllinesServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
